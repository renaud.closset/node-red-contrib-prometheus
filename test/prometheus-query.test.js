var assert = require('assert');
var axios = require('../src/axios-instance');
var moxios = require('moxios');
var queryNode = require('../src/prometheus-query');
var helper = require('node-red-node-test-helper');
var sinon = require('sinon');

helper.init(require.resolve('node-red'))

describe('prometheus-query', function() {

    beforeEach(function () {
        moxios.install(axios);
    })

    afterEach(function () {
        moxios.uninstall(axios);
    })

    describe('payload input selection', function() {

        it('should use msg.payload if nothing else is defined', function() {
                       
            var flow = [{ id: "pq", type: "prometheus-query", name: "test name" }];
            helper.load(queryNode, flow, function () {
                var pq = getNode("pq");
                pq.receive({ payload: "metrics{label='value'}" });

                moxios.wait(function () {
                    let request = moxios.requests.mostRecent();
                    console.log(request);
                    // assert.equal(request.path, 'query');
                });
            });
            
        });
    });
});