module.exports = function(RED) {

    function PrometheusSeries(config) {
        // Initialize the features shared by all nodes
        RED.nodes.createNode(this, config);

        const server = RED.nodes.getNode(config.server);

        var node = this;
        
        node.on('input', function(msg) {
            // normalize "match" to an array with input sanitation
            let match = config.match || msg.payload.hasOwnProperty('match') ? msg.payload.match : msg.payload;
            if (typeof match === "string") match = match.trim().replace(/\s+/g, ' ').split(/[,;\s]/);

            if (match.length === 0) {
                node.error("At least one match must be provided");
                return;
            }
            
            const params = {
                match,
                start: config.start || msg.payload.start || null,
                end: config.timeout || msg.payload.end || null
            };
            server.instance.get('series', { params })
                .then(msg => node.send(msg))
                .catch(error => {
                    msg.params = params;
                    msg.url = server.instance.getUri({url: 'series', params });
                    msg.data = error.response.data;
                    node.error(error.message, msg);
                });
        });

        return;
    }

    RED.nodes.registerType("prometheus-series", PrometheusSeries);
}
