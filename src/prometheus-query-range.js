module.exports = function(RED) {

    function PrometheusQueryRange(config)
    {
        // Initialize the features shared by all nodes
        RED.nodes.createNode(this, config);

        // Retrieve this node's configuration node
        const server = RED.nodes.getNode(config.server);

        var node = this;
        
        node.on('input', function(msg) {
            const params = { 
                query: config.query || msg.payload.query || msg.payload, 
                start: config.start || null, 
                end: config.end || null, 
                step: config.step || null, 
                timeout: config.timeout || null
            };
            server.instance.get('query_range', { params })
                .then(msg => node.send(msg))
                .catch(error => node.error(error.message, msg));
        });
    }

    RED.nodes.registerType("prometheus-query-range", PrometheusQueryRange);
}
