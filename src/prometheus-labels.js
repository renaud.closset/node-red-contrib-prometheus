module.exports = function(RED) {

    function PrometheusLabels(config) {
        // Initialize the features shared by all nodes
        RED.nodes.createNode(this, config);

        const server = RED.nodes.getNode(config.server);

        var node = this;
        
        node.on('input', function(msg) {
            // normalize "match" to an array with input sanitation
            let match = config.match || msg.payload.hasOwnProperty('match') ? msg.payload.match : msg.payload;
            if (typeof match === "string") match = match.trim().replace(/\s+/g, ' ').split(/[,;\s]/);
            
            const params = {
                match,
                start: config.start || msg.payload.start || null,
                end: config.timeout || msg.payload.end || null
            };
            server.instance.get('labels', { params })
                .then(msg => node.send(msg))
                .catch(error => {
                    msg.params = params;
                    msg.url = server.instance.getUri({url: 'labels', params });
                    msg.data = error.response.data;
                    node.error(error.message, msg);
                });
        });
    }

    RED.nodes.registerType("prometheus-labels", PrometheusLabels);
}
