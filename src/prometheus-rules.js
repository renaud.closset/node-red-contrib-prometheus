module.exports = function(RED) {

    function PrometheusRules(config) {
        // Initialize the features shared by all nodes
        RED.nodes.createNode(this, config);

        const server = RED.nodes.getNode(config.server);

        var node = this;
        
        node.on('input', function(msg) {
                       
            const params = {
                "type": config.ruleType || msg.payload.ruleType || null
            };
            server.instance.get('rules', { params })
                .then(msg => node.send(msg))
                .catch(error => {
                    msg.params = params;
                    msg.url = server.instance.getUri({url: 'rules', params });
                    msg.data = error.response.data;
                    node.error(error.message, msg);
                });
        });

        return;
    }

    RED.nodes.registerType("prometheus-rules", PrometheusRules);
}
