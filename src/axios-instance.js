let axios = require('axios'),
  instance = axios.create();

instance.interceptors.response.use(function (r) {
    return {
        payload: r.data.data,
        status: r.data.status,
        warnings: r.data.warning || []
    };
});

module.exports = instance;