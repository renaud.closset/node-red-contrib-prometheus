module.exports = function(RED) {

    const axiosInstance = require('./axios-instance');

    function PrometheusServerNode(n) {

        RED.nodes.createNode(this, n);

        // create axios configuration
        let axiosConfig = {
            baseURL: n.host + ':' + n.port + '/api/v1/',
            headers: {
                'X-Node-Red': 'prometheus'
            }
        };

        // add credentials to axios instance if required
        if (n.username) {
            axiosConfig.auth = {
                username: n.username,
                password: n.password
            }
        }

        this.axiosConfig = axiosConfig;
    }

    RED.nodes.registerType("prometheus-server", PrometheusServerNode);
}