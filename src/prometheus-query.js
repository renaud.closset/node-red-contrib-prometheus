module.exports = function(RED) {

    function PrometheusQuery(config) {
        // Initialize the features shared by all nodes
        RED.nodes.createNode(this, config);

        const server = RED.nodes.getNode(config.server);

        var node = this;
        
        node.on('input', function(msg) {
            const params = {
                query: config.query || msg.payload.query || msg.payload,
                time: config.time || msg.payload.time || null,
                timeout: config.timeout || msg.payload.timeout || null
            };
            server.instance.get('query', { params })
                .then(msg => node.send(msg))
                .catch(error => node.error(error.message, msg));
        });
    }

    RED.nodes.registerType("prometheus-query", PrometheusQuery);
}
