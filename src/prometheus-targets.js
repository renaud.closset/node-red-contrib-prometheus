module.exports = function(RED) {

    function PrometheusTargets(config) {
        // Initialize the features shared by all nodes
        RED.nodes.createNode(this, config);

        const server = RED.nodes.getNode(config.server);

        var node = this;
        
        node.on('input', function(msg) {
                       
            const params = {
                state: config.state || msg.payload.state || null
            };
            server.instance.get('targets', { params })
                .then(msg => node.send(msg))
                .catch(error => {
                    msg.params = params;
                    msg.url = server.instance.getUri({url: 'targets', params });
                    msg.data = error.response.data;
                    node.error(error.message, msg);
                });
        });

        return;
    }

    RED.nodes.registerType("prometheus-targets", PrometheusTargets);
}
